﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reflection.src
{
    public class F
    {
        public F() { }
        public int I1 { get; set; }

        public int I2 { get; set; }

        public int I3 { get; set; }

        public int I4 { get; set; }

        public int I5 { get; set; }

        public F(int i1, int i2, int i3, int i4, int i5)
        {
            this.I1 = i1;
            this.I2 = i2;
            this.I3 = i3;
            this.I4 = i4;
            this.I5 = i5;
        }
    }
}
