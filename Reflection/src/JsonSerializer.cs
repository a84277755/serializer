﻿using Newtonsoft.Json;
using Reflection.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reflection.src
{
    class JsonSerializer<T> : ISerializer<T>
    {
        public T Deserialize(string objString)
        {
            return JsonConvert.DeserializeObject<T>(objString);
        }

        public string Serialize(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
