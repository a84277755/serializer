﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reflection.src.Interfaces
{
    interface ISerializer<T>
    {

        public string Serialize(T obj);
        public T Deserialize(string objString);
    }
}
