﻿using Newtonsoft.Json;
using Reflection.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;

namespace Reflection.src
{
    class CsvSerializer<T> : ISerializer<T>
    {
        public T Deserialize(string objString)
        {
            var lines = objString.Split(Environment.NewLine);
            var titles = lines[0].Split(";");
            var values = lines[1].Split(";");

            Type t = typeof(T);
            object result = Activator.CreateInstance(t);

            for (int i = 0; i < titles.Length; i++)
            {
                var property = t.GetProperty(titles[i], BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                property.SetValue(result, int.Parse(values[i]));
            }

            return (T)result;
        }

        public string Serialize(T obj)
        {
            var classToSerializeType = typeof(T);
            PropertyInfo[] fields = classToSerializeType.GetProperties();

            string[] titles = new string[fields.Length];
            object[] values = new object[fields.Length];

            for (int i = 0; i < fields.Length; i++)
            {
                string name = fields[i].Name;
                object value = classToSerializeType.GetProperty(name).GetValue(obj);
                
                titles[i] = name;
                values[i] = value;
            }

            return String.Join(";", titles) + Environment.NewLine + String.Join(";", values);
        }
    }
}
