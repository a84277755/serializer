﻿using Reflection.src;
using System;
using System.IO;

namespace Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            const int NUMBER_OF_ITERATIONS = 100_000;

            var f = new F(1, 2, 3, 4, 5);
            var csvSerializer = new CsvSerializer<F>();
            var jsonSerializer = new JsonSerializer<F>();

            // To CSV (without console output)

            string csvString = csvSerializer.Serialize(f);
            Console.WriteLine(csvString);

            var startTime = DateTime.Now.Ticks;

            for (var i = 0; i < NUMBER_OF_ITERATIONS; i++)
            {
                csvSerializer.Serialize(f);
            }

            var endTime = DateTime.Now.Ticks;
            var serializeToCsvTime = (endTime - startTime) / TimeSpan.TicksPerMillisecond;
            
            // To CSV (with console output)

            startTime = DateTime.Now.Ticks;

            for (var i = 0; i < NUMBER_OF_ITERATIONS; i++)
            {
                var draftString = csvSerializer.Serialize(f);
                Console.WriteLine(draftString);
            }

            endTime = DateTime.Now.Ticks;
            var serializeToCsvWithConsoleTime = (endTime - startTime) / TimeSpan.TicksPerMillisecond;

            // To Json (without console output)

            var jsonString = jsonSerializer.Serialize(f);
            Console.WriteLine(jsonString);

            startTime = DateTime.Now.Ticks;

            for (var i = 0; i < NUMBER_OF_ITERATIONS; i++)
            {
                jsonSerializer.Serialize(f);
            }

            endTime = DateTime.Now.Ticks;
            var serializeToJsonWithoutConsoleTime = (endTime - startTime) / TimeSpan.TicksPerMillisecond;

            // To Json (with console output)
            startTime = DateTime.Now.Ticks;

            for (var i = 0; i < NUMBER_OF_ITERATIONS; i++)
            {
                var draftString = jsonSerializer.Serialize(f);
                Console.WriteLine(draftString);
            }

            endTime = DateTime.Now.Ticks;
            var serializeToJsonWithConsoleTime = (endTime - startTime) / TimeSpan.TicksPerMillisecond;

            // DECERIALIZATION

            // Decerialize CSV to class
            var csvDataFromFile = File.ReadAllText("src" + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "csv.csv");

            var fDecerializedFromCsv = csvSerializer.Deserialize(csvDataFromFile);
            var fDecerializedFromCsvResult = $"{fDecerializedFromCsv.I1} {fDecerializedFromCsv.I2} {fDecerializedFromCsv.I3} {fDecerializedFromCsv.I4} {fDecerializedFromCsv.I5}";

            startTime = DateTime.Now.Ticks;

            for (var i = 0; i < NUMBER_OF_ITERATIONS; i++)
            {
                csvSerializer.Deserialize(csvDataFromFile);
            }

            endTime = DateTime.Now.Ticks;
            var decerializeFromCsvTime = (endTime - startTime) / TimeSpan.TicksPerMillisecond;
            Console.WriteLine("Result of deserializing from CSV: " + fDecerializedFromCsvResult);

            // Decerialize JSON to class
            var jsonDataFromFile = File.ReadAllText("src" + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "json.json");
            var fDeserializedFromJson = jsonSerializer.Deserialize(jsonDataFromFile);

            var fDeserializedFromJsonResult = $"{fDeserializedFromJson.I1} {fDeserializedFromJson.I2} {fDeserializedFromJson.I3} {fDeserializedFromJson.I4} {fDeserializedFromJson.I5}";
            Console.WriteLine("Result of deserializing from JSON: " + fDeserializedFromJsonResult);

            startTime = DateTime.Now.Ticks;

            for (var i = 0; i < NUMBER_OF_ITERATIONS; i++)
            {
                jsonSerializer.Deserialize(jsonDataFromFile);
            }

            endTime = DateTime.Now.Ticks;
            var decerializeFromJsonTime = (endTime - startTime) / TimeSpan.TicksPerMillisecond;

            // result

            Console.WriteLine("Spent time in ms (serialization to CSV): " + serializeToCsvTime);
            Console.WriteLine("Spent time in ms (serialization to CSV with console): " + serializeToCsvWithConsoleTime);
            Console.WriteLine("Spent time in ms (serialization to JSON): " + serializeToJsonWithoutConsoleTime);
            Console.WriteLine("Spent time in ms (serialization to JSON with console): " + serializeToJsonWithConsoleTime);
            Console.WriteLine("Spent time in ms (decerialize from CSV): " + decerializeFromCsvTime);
            Console.WriteLine("Spent time in ms (decerialize from JSON): " + decerializeFromJsonTime);
        }
    }
}
